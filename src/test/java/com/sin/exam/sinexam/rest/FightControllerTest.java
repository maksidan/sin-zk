package com.sin.exam.sinexam.rest;

import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.entites.Troop;
import com.sin.exam.sinexam.repository.fight.FightRepository;
import com.sin.exam.sinexam.repository.troop.TroopRepository;
import com.sin.exam.sinexam.services.FightService;
import com.sin.exam.sinexam.services.TroopService;
import com.sin.exam.sinexam.services.imp.FightServiceImp;
import com.sin.exam.sinexam.services.imp.TroopServiceImp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class FightServiceTest {

    FightService fightService;
    TroopService troopService;
    FightRepository fightRepository;
    TroopRepository troopRepository;

    @BeforeEach
    void setUp() {
        fightRepository = new FightRepository();
        troopRepository = new TroopRepository();

        fightService = new FightServiceImp(troopRepository, fightRepository);
        troopService = new TroopServiceImp(troopRepository);
    }

    @Test
    void testGoodTroopWin() {
        Troop goodTroop = new Troop();
        goodTroop.setHeroes(new ArrayList<>());
        goodTroop.setEvil(false);
        Troop evilTroop = new Troop();
        evilTroop.setHeroes(new ArrayList<>());
        evilTroop.setEvil(true);

        Hero hero1 = new Hero();
        hero1.setName("A");
        hero1.setEvil(false);
        hero1.setStrength(100);
        hero1.setMagic(100);
        hero1.setWill(100);

        Hero hero2 = new Hero();
        hero2.setName("B");
        hero2.setEvil(true);
        hero2.setStrength(10);
        hero2.setMagic(10);
        hero2.setWill(10);

        goodTroop.getHeroes().add(hero1);
        evilTroop.getHeroes().add(hero2);

        Assertions.assertEquals(goodTroop, fightService.startFight(goodTroop, evilTroop));
    }

    @Test
    void testEvilTroopWin() {
        Troop goodTroop = new Troop();
        goodTroop.setHeroes(new ArrayList<>());
        goodTroop.setEvil(false);
        Troop evilTroop = new Troop();
        evilTroop.setHeroes(new ArrayList<>());
        evilTroop.setEvil(true);

        Hero hero1 = new Hero();
        hero1.setName("A");
        hero1.setEvil(false);
        hero1.setStrength(10);
        hero1.setMagic(10);
        hero1.setWill(10);

        Hero hero2 = new Hero();
        hero2.setName("B");
        hero2.setEvil(true);
        hero2.setStrength(100);
        hero2.setMagic(100);
        hero2.setWill(100);

        goodTroop.getHeroes().add(hero1);
        evilTroop.getHeroes().add(hero2);

        Assertions.assertEquals(evilTroop, fightService.startFight(goodTroop, evilTroop));
    }

    @Test
    void testDraw() {
        Troop goodTroop = new Troop();
        goodTroop.setHeroes(new ArrayList<>());
        goodTroop.setEvil(false);
        Troop evilTroop = new Troop();
        evilTroop.setHeroes(new ArrayList<>());
        evilTroop.setEvil(true);

        Hero hero1 = new Hero();
        hero1.setName("A");
        hero1.setEvil(false);
        hero1.setStrength(10);
        hero1.setMagic(10);
        hero1.setWill(10);

        Hero hero2 = new Hero();
        hero2.setName("B");
        hero2.setEvil(true);
        hero2.setStrength(10);
        hero2.setMagic(10);
        hero2.setWill(10);

        goodTroop.getHeroes().add(hero1);
        evilTroop.getHeroes().add(hero2);

        Assertions.assertEquals(goodTroop, fightService.startFight(goodTroop, evilTroop));
    }
}
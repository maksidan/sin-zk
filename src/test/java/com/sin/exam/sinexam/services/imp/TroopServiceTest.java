package com.sin.exam.sinexam.services.imp;

import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.entites.Troop;
import com.sin.exam.sinexam.repository.troop.TroopRepository;
import com.sin.exam.sinexam.services.TroopService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class TroopServiceTest {

    TroopService troopService;
    TroopRepository troopRepository;
    Troop troop;

    @BeforeEach
    void setUp() {
        troopRepository = new TroopRepository();
        troopService = new TroopServiceImp(troopRepository);

        troop = new Troop();
        troop.setEvil(false);
        troop.setHeroes(new ArrayList<>());

        Hero firstHero = new Hero();
        firstHero.setEvil(false);
        firstHero.setName("Elon Musk");

        Hero secondHero = new Hero();
        secondHero.setEvil(false);
        secondHero.setName("Iron Man");

        troopService.addHero(firstHero, troop);
        troopService.addHero(secondHero, troop);
    }

    @Test
    void testAddHeroWithoutUniqueName() {
        Hero testHero = new Hero();
        testHero.setEvil(false);
        testHero.setName("Elon Musk");

        Assertions.assertThrows(IllegalArgumentException.class, () -> troopService.addHero(testHero, troop));
        Assertions.assertFalse(troop.getHeroes().contains(testHero));
    }

    @Test
    void testAddHeroDifferentSide() {
        Hero testHero = new Hero();
        testHero.setEvil(true);
        testHero.setName("Jeff Bezos");

        Assertions.assertThrows(IllegalArgumentException.class, () -> troopService.addHero(testHero, troop));
        Assertions.assertFalse(troop.getHeroes().contains(testHero));
    }

    @Test
    void testAddHeroSuccess() {
        Hero testHero = new Hero();
        testHero.setEvil(false);
        testHero.setName("Jeff Bezos");

        troopService.addHero(testHero, troop);
        Assertions.assertTrue(troop.getHeroes().contains(testHero));
    }
}
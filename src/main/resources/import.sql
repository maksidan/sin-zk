INSERT INTO hero (name, is_evil, strength, will, magic) VALUES
   ('Gandalf', FALSE, 2, 5, 10),
   ('Bilbo', FALSE, 3, 10, 1),
   ('Thorin', FALSE, 10, 5, 0),
   ('Bolg', TRUE, 10, 10, 0),
   ('Golfimbul', TRUE, 6, 4, 0),
   ('Necromancer', TRUE, 3, 3, 10);

INSERT INTO troop (is_evil) VALUES
    (TRUE),
    (FALSE);

INSERT INTO troop_heroes VALUES
    (1, 1), (1, 2), (1, 3),
    (2, 4), (2, 5), (2, 6);

INSERT INTO hero (name, is_evil, strength, will, magic) VALUES
    ('Iron Man', FALSE, 6, 4, 0);

INSERT INTO troop (is_evil) VALUES
    (TRUE),
    (FALSE),
    (FALSE);

INSERT INTO troop_heroes VALUES
    (3, 2),
    (4, 5),
    (5, 7);
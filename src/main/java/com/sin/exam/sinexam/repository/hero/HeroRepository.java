package com.sin.exam.sinexam.repository.hero;

import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.repository.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class HeroRepository extends BaseDao<Hero> {

    public HeroRepository() {
        super(Hero.class);
    }
}

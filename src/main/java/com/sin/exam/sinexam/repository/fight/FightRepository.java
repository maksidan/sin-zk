package com.sin.exam.sinexam.repository.fight;

import com.sin.exam.sinexam.entites.Fight;
import com.sin.exam.sinexam.repository.BaseDao;
import org.springframework.stereotype.Repository;

@Repository
public class FightRepository extends BaseDao<Fight> {

    public FightRepository() {
        super(Fight.class);
    }
}

package com.sin.exam.sinexam.repository;

import jakarta.persistence.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Slf4j
public abstract class BaseDao<T> implements GenericDao<T> {

    @PersistenceContext
    protected EntityManager em;

    protected final Class<T> type;

    protected BaseDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public T find(Long id) {
        Objects.requireNonNull(id);
        return em.find(type, id);
    }

    @Override
    public List<T> findAll() {
        try {
            return em.createQuery("SELECT e FROM " + type.getSimpleName() + " e", type).getResultList();
        } catch (RuntimeException e) {
            log.error("Error while finding entities of type: " + type.getSimpleName() + "\n" + e.getMessage());
            throw new PersistenceException(e);
        }
    }

    @Override
    public void persist(T entity) {
        Objects.requireNonNull(entity);
        try {
            em.persist(entity);
        } catch (RuntimeException e) {
            log.error("Error while persisting entity of type: " + type.getSimpleName() + "\n" + e.getMessage());
            throw new PersistenceException(e);
        }
    }

    @Override
    public void persist(Collection<T> entities) {
        Objects.requireNonNull(entities);
        if (entities.isEmpty()) {
            log.warn("Received empty list of " + type.getSimpleName() + " for persisting");
            return;
        }
        try {
            entities.forEach(this::persist);
        } catch (RuntimeException e) {
            log.error("Error while persisting entities of type: " + type.getSimpleName() + "\n" + e.getMessage());
            throw new PersistenceException(e);
        }
    }

    @Override
    public T update(T entity) {
        Objects.requireNonNull(entity);
        try {
            return em.merge(entity);
        } catch (RuntimeException e) {
            log.error("Error while updating entity of type: " + type.getSimpleName() + "\n" + e.getMessage());
            throw new PersistenceException(e);
        }
    }

    @Override
    public void remove(T entity) {
        Objects.requireNonNull(entity);
        try {
            final T toRemove = em.merge(entity);
            if (toRemove != null) {
                em.remove(toRemove);
            }
        } catch (RuntimeException e) {
            log.error("Error while removing entity of type: " + type.getSimpleName() + "\n" + e.getMessage());
            throw new PersistenceException(e);
        }
    }

    @Override
    public boolean exists(Long id) {
        return id != null && em.find(type, id) != null;
    }

    public EntityManager getEm() {
        return em;
    }
}

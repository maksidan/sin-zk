package com.sin.exam.sinexam.repository.troop;

import com.sin.exam.sinexam.entites.Fight;
import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.entites.Troop;
import com.sin.exam.sinexam.repository.BaseDao;
import jakarta.persistence.PersistenceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class TroopRepository extends BaseDao<Troop> {

    public TroopRepository() {
        super(Troop.class);
    }

    public List<Hero> findAllAssociatedHeroes(Troop troop) {
        try {
            return troop.getHeroes();
        } catch (RuntimeException e) {
            log.error("Error while getting heroes list from troop: " + e.getMessage());
            throw new PersistenceException(e);
        }
    }

    public void addHero(Hero hero, Troop troop) {
        try {
            troop.getHeroes().add(hero);
        } catch (RuntimeException e) {
            log.error("Error while adding hero to a troop: " + e.getMessage());
            throw new PersistenceException();
        }
    }

    public void addFight(Fight fight, Troop troop) {
        try {
            troop.getFights().add(fight);
        } catch (RuntimeException e) {
            log.error("Error while adding fight to a troop: " + e.getMessage());
            throw new PersistenceException();
        }
    }
}

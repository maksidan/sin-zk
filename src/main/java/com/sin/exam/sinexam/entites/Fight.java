package com.sin.exam.sinexam.entites;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "fight")
@Getter @Setter
public class Fight extends AbstractEntity {

    @Column(name = "based_on", nullable = false)
    private PowerTypesEnum basedOn;

    @Column(name = "evil_won", nullable = false)
    private boolean evilWon;

    @OneToOne
    private Troop goodTroop;

    @OneToOne
    private Troop evilTroop;
}

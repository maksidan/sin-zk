package com.sin.exam.sinexam.entites;

import java.util.List;
import java.util.Random;

public enum PowerTypesEnum {
    STRENGTH,
    WILL,
    MAGIC;

    private static final List<PowerTypesEnum> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static PowerTypesEnum getRandomPowerType()  {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}

package com.sin.exam.sinexam.entites;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "troop")
@Getter @Setter
public class Troop extends AbstractEntity{

    @Column(name = "is_evil", nullable = false)
    private boolean isEvil;

    @ManyToMany(cascade = CascadeType.PERSIST)
    private List<Hero> heroes;

    @OneToMany
    private List<Fight> fights;
}

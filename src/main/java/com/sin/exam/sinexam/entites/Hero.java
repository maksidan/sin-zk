package com.sin.exam.sinexam.entites;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "hero")
@Getter @Setter
public class Hero extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "is_evil", nullable = false)
    private boolean isEvil;

    @Column(name = "strength")
    private Integer strength;

    @Column(name = "will")
    private Integer will;

    @Column(name = "magic")
    private Integer magic;

    @ManyToMany(mappedBy = "heroes", cascade = CascadeType.PERSIST)
    private List<Troop> troops;
}

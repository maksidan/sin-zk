package com.sin.exam.sinexam.rest;

import com.sin.exam.sinexam.entites.Troop;
import com.sin.exam.sinexam.services.FightService;
import com.sin.exam.sinexam.services.TroopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/fight")
public class FightController {

    private final FightService fightService;
    private final TroopService troopService;

    @Autowired
    public FightController(FightService fightService, TroopService troopService) {
        this.fightService = fightService;
        this.troopService = troopService;
    }

    @PutMapping("/start")
    @Transactional
    public void postFight(@RequestParam Long firstTroopId,
                          @RequestParam Long secondTroopId) {
        Troop firstTroop = troopService.findById(firstTroopId);
        Troop secondTroop = troopService.findById(secondTroopId);

        if (firstTroop.isEvil()) {
            fightService.startFight(secondTroop, firstTroop);
        } else {
            fightService.startFight(firstTroop, secondTroop);
        }
    }
}

package com.sin.exam.sinexam.rest;

import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.repository.hero.HeroRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/hero")
public class HeroController {

    HeroRepository heroRepository;

    @Autowired
    public HeroController(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    @GetMapping(value = "/{id}")
    Hero getHero(@PathVariable Long id) {
        return heroRepository.find(id);
    }

    @PostMapping
    @Transactional
    public void postHero(@RequestBody Hero hero) {
        heroRepository.persist(hero);
    }

    @PutMapping("/{id}")
    @Transactional
    public Hero putHero(@PathVariable Long id, @RequestBody Hero newHero) {
        Hero currentHero = heroRepository
                .find(id);
        if (currentHero == null) {
            throw new IllegalArgumentException("Hero with id " + id + " was not found");
        } else {
            heroRepository.remove(currentHero);
            heroRepository.persist(newHero);
        }
        return heroRepository.find(id);
    }
}

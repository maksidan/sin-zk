package com.sin.exam.sinexam.services;

import com.sin.exam.sinexam.entites.Troop;

public interface FightService {

    Troop startFight(Troop goodTroop, Troop evilTroop);
}

package com.sin.exam.sinexam.services;

import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.entites.Troop;

public interface TroopService {

    void addHero(Hero hero, Troop troop);

    Troop findById(Long id);
}

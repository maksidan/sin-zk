package com.sin.exam.sinexam.services.imp;

import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.entites.Troop;
import com.sin.exam.sinexam.repository.troop.TroopRepository;
import com.sin.exam.sinexam.services.TroopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TroopServiceImp implements TroopService {

    private final TroopRepository troopRepository;

    @Autowired
    public TroopServiceImp(TroopRepository troopRepository) {
        this.troopRepository = troopRepository;
    }

    @Override
    public void addHero(Hero newHero, Troop troop) {
        log.info("Adding new hero: " + newHero.toString() + " to troop: " + troop.toString());

        List<Hero> heroes = troopRepository.findAllAssociatedHeroes(troop);
        List<String> heroesNames = heroes.stream()
                .map(Hero::getName)
                .collect(Collectors.toList());

        if (heroesNames.contains(newHero.getName())) {
            log.error("Cant add hero to a troop due to repeating name");
            throw new IllegalArgumentException("Heroes name must be unique for his troop!");
        }

        if (troop.isEvil() != newHero.isEvil()) {
            log.error("Cant add hero to a troop due to sides difference");
            throw new IllegalArgumentException("Hero must take the same side as the troop!");
        }

        troopRepository.addHero(newHero, troop);
    }

    @Override
    public Troop findById(Long id) {
        return troopRepository.find(id);
    }
}

package com.sin.exam.sinexam.services.imp;

import com.sin.exam.sinexam.entites.Fight;
import com.sin.exam.sinexam.entites.Hero;
import com.sin.exam.sinexam.entites.PowerTypesEnum;
import com.sin.exam.sinexam.entites.Troop;
import com.sin.exam.sinexam.repository.fight.FightRepository;
import com.sin.exam.sinexam.repository.troop.TroopRepository;
import com.sin.exam.sinexam.services.FightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FightServiceImp implements FightService {

    private final TroopRepository troopRepository;
    private final FightRepository fightRepository;

    @Autowired
    public FightServiceImp(TroopRepository troopRepository, FightRepository fightRepository) {
        this.troopRepository = troopRepository;
        this.fightRepository = fightRepository;
    }

    @Override
    public Troop startFight(Troop goodTroop, Troop evilTroop) {
        log.info("Starting a fight between: " + goodTroop.toString() + " and " + evilTroop.toString());
        if (goodTroop.isEvil() == evilTroop.isEvil()) {
            log.error("Fight failed because troops are on the same side.");
            throw new IllegalArgumentException("Two troops on the same side cannot start a fight!");
        }

        Fight fight = new Fight();
        fight.setGoodTroop(goodTroop);
        fight.setEvilTroop(evilTroop);
        if (troopRepository.getEm() != null) {
            troopRepository.addFight(fight, goodTroop);
            troopRepository.addFight(fight, evilTroop);
        }

        PowerTypesEnum fightPowerType = PowerTypesEnum.getRandomPowerType();
        fight.setBasedOn(fightPowerType);

        Pair<Integer, Integer> powers = getSidesPowers(goodTroop, evilTroop, fightPowerType);
        Integer goodPower = powers.getFirst();
        Integer evilPower = powers.getSecond();

        if (goodPower > evilPower) {
            fight.setEvilWon(false);
            log.info("Good troop won!");
            if (fightRepository.getEm() != null) fightRepository.persist(fight);
            return goodTroop;
        } else if (goodPower < evilPower) {
            fight.setEvilWon(true);
            log.info("Evil troop won :(");
            if (fightRepository.getEm() != null) fightRepository.persist(fight);
            return evilTroop;
        } else {
            fight.setEvilWon(false);
            log.info("Its a draw!");
            log.info("Well, evil is dead, so powers of good won!");
            if (fightRepository.getEm() != null) fightRepository.persist(fight);
            return goodTroop;
        }
    }

    private Pair<Integer, Integer> getSidesPowers(Troop goodTroop, Troop evilTroop, PowerTypesEnum fightPowerType) {
        int goodPower = 0, evilPower = 0;

        switch (fightPowerType) {
            case STRENGTH:
                goodPower = goodTroop.getHeroes().stream()
                        .mapToInt(Hero::getStrength)
                        .sum();
                evilPower = evilTroop.getHeroes().stream()
                        .mapToInt(Hero::getStrength)
                        .sum();
                break;
            case WILL:
                goodPower = goodTroop.getHeroes().stream()
                        .mapToInt(Hero::getWill)
                        .sum();
                evilPower = evilTroop.getHeroes().stream()
                        .mapToInt(Hero::getWill)
                        .sum();
                break;
            case MAGIC:
                goodPower = goodTroop.getHeroes().stream()
                        .mapToInt(Hero::getMagic)
                        .sum();
                evilPower = evilTroop.getHeroes().stream()
                        .mapToInt(Hero::getMagic)
                        .sum();
                break;
        }

        return Pair.of(goodPower, evilPower);
    }
}

package com.sin.exam.sinexam;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SinExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(SinExamApplication.class, args);
	}
}
